package br.com.benjamim.MicroServices.service.ClienteService;

import br.com.benjamim.MicroServices.Repository.ClienteRepository;
import br.com.benjamim.MicroServices.domain.main.Cliente;
import br.com.benjamim.MicroServices.domain.ClientePutRequestBody.ClientePostRequestBody;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.HttpStatus;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RequiredArgsConstructor
@Service
@Log4j2
public class ClienteService {

    private final ClienteRepository clienteRepository;
    private final JdbcTemplate jdbcTemplate;

    public List<Cliente> queryFindAll() {
        return jdbcTemplate.query("select * from Cliente", (rs, rowNum) ->
                new Cliente(rs.getLong("id"),
                        rs.getString("nome"),
                        rs.getString("cpf")));
    }

    public List<Cliente> queryFindByID(Long id) {
        return jdbcTemplate.query("select * from Cliente where id = "+id, (rs, rowNum) ->
                new Cliente(rs.getLong("id"),
                        rs.getString("nome"),
                        rs.getString("cpf")));
    }

    //Consutar todos CLientes.
    public List<Cliente> findAll() {
        return clienteRepository.findAll();
    }

    //Consutar Cliente por id.
    public Cliente finfById(Long id) {
        return clienteRepository.findById(id)
                .orElseThrow(() -> new ResponseStatusException(
                        HttpStatus.BAD_REQUEST, "Cliente nao encontrado"));
    }

    //Salvar um cliente.
    public void save(ClientePostRequestBody clientePostRequestBody) {
        clienteRepository.save(Cliente.builder()
                .id(clientePostRequestBody.getId())
                .nome(clientePostRequestBody.getNome())
                .cpf(clientePostRequestBody.getCpf())
                .build());
    }

    //Editar um cliente
    public void replace(ClientePostRequestBody clientePostRequestBody) {
        Cliente cliente = Cliente.builder()
                .nome(clientePostRequestBody.getNome())
                .id(clientePostRequestBody.getId())
                .cpf(clientePostRequestBody.getCpf()).build();
        clienteRepository.save(cliente);
    }

    //Deletar um cliente por Id.
    public void delete(Long id) {
        clienteRepository.delete(clienteRepository.findById(id).orElse(null));
    }


}
