package br.com.benjamim.MicroServices.domain.ClientePutRequestBody;

import lombok.Data;

@Data
public class ClientePostRequestBody {
    private Long id;
    private String nome;
    private String cpf;
}
