package br.com.benjamim.MicroServices;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TestDxcApplication {

	public static void main(String[] args) {
		SpringApplication.run(TestDxcApplication.class, args);
	}

}
