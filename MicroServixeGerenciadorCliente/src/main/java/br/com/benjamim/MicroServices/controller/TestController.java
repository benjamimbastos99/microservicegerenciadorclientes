package br.com.benjamim.MicroServices.controller;

import br.com.benjamim.MicroServices.domain.main.Cliente;
import br.com.benjamim.MicroServices.domain.ClientePutRequestBody.ClientePostRequestBody;
import br.com.benjamim.MicroServices.service.ClienteService.ClienteService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/")
@RequiredArgsConstructor
public class TestController {

    private final ClienteService clienteService;

    @GetMapping
    public List<Cliente> queryFindAll() {
        return clienteService.queryFindAll();
    }

    @GetMapping(path = "/{id}")
    public List<Cliente> queryFindByID(@PathVariable Long id) {
        return clienteService.queryFindByID(id);
    }

//    @GetMapping
//    public List<Cliente> findAll() {
//        return clienteService.findAll();
//    }
//
//    @GetMapping(path = "/{id}")
//    public Cliente findById(@PathVariable Long id) {
//        return clienteService.finfById(id);
//    }

    @PostMapping
    private void save(@RequestBody ClientePostRequestBody cliente) {
        clienteService.save(cliente);
    }

    @PutMapping
    public void replace(@RequestBody ClientePostRequestBody clientePostRequestBody) {
        clienteService.replace(clientePostRequestBody);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable Long id) {
        clienteService.delete(id);
    }
}
