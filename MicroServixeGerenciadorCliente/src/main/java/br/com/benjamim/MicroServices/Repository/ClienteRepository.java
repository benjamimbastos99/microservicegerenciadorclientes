package br.com.benjamim.MicroServices.Repository;

import br.com.benjamim.MicroServices.domain.main.Cliente;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ClienteRepository extends JpaRepository<Cliente, Long> {
}
